# LoG12-datascience

Slides and Jupyter Notebooks from my Talk on Data Science with Python at League of Geeks #12

## Running example code

1. Ensure you have python installed
2. Go to `python` directory
3. Execute `python -m venv venv` to set up a clean virtual environment
4. Run `pip install -r requirements.txt` to install all required packages
5. To run the Jupyter notebook server
   1. Activate virtual environment `venv/bin/activate`
   2. Run Jupyter Server `jupyter notebook`